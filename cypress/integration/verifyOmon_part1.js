import login from '../support/PageObjects/utcLogin.js'
import omon from '../support/PageObjects/omon.js';
import oovr from '../support/PageObjects/oovr.js';
import eqs from '../support/PageObjects/equipmentSummary.js';
import common from '../support/PageObjects/common.js';

//var data = require('../../fixtures/otuConfig.json');

const Junction1_Control_Bits = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'TS', 'EP'];

const Junction1_Reply_Bits = ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'CS', 'LF1', 'RF1', 
'RF2', 'DF', 'EC', 'BD1', 'BD2', 'BD3', 'BD4', 'PR0', 'PR1'];


  describe('verify OMON - ', function () {
    // before(() => {
    // });


    it('Part1:0 Login to UTC', function(){
      login.login('/utc');
      cy.viewport(1999, 1000);
      cy.log('User logged in successfully');
      // tidy up after any broken tests
      //common.runCommand('XLIV X04/003');
    })

    /*
    it('Part1:1 Verify Outstation historical records', function () {
      cy.log("Verify Outstation historical records: RTO-1433 steps 1-3");
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');  
      cy.get(omon.omonFirstRow).should('be.visible');
      cy.get('.ag-row-first').should('be.visible');
      cy.get('[row-index="1"]').should('be.visible');
      cy.get('.ag-body-viewport').scrollTo('bottom');
      cy.get('.ag-row-last').should('be.visible');


      for (let count = 0; count < 100; count ++)
      {
        cy.get('.ag-row-last').invoke('attr', 'row-index').then((index) => {
          cy.log('index = [' + index + ']');
          if (index == 99) 
          {
            // break out of the loop
            //break;
            count = 100;
          }
          else
          {
            cy.wait(5000);
            cy.get('.ag-body-viewport').scrollTo('bottom');
          }
        })
      }
      cy.get('.ag-row-last').invoke('attr', 'row-index').should('eq', '99');        
    });


    it('Part1:2 Verify Control and Reply headers are displayed', function () {
      cy.log('Verify Control and Reply headers are displayed');
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');  
        cy.get('[col-id="control-group_0"] > .ag-header-group-cell-label').should('contain.text', 'Control');
        cy.get('[col-id="reply-group_0"] > .ag-header-group-cell-label').should('contain.text', 'Reply');
    });


    it('Part1:3 Verify that the control and reply texts are shown left to right in ascending order', function () {
      cy.log('Verify that the control and reply texts are shown left to right in ascending order');
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');  
      cy.get("[col-id='0-F1'][role='columnheader']").should('be.visible');
      cy.get("[col-id='1-F2'][role='columnheader']").should('be.visible');
      cy.get("[col-id='2-F3'][role='columnheader']").should('be.visible');
      cy.get("[col-id='0-G1'][role='columnheader']").should('be.visible');
      cy.get("[col-id='1-G2'][role='columnheader']").should('be.visible');
      cy.get("[col-id='2-G3'][role='columnheader']").should('be.visible');
    });


    it('Part1:4 Verify the contents of Control and Reply texts', function () {
      cy.log('Verify the contents of Control and Reply texts');
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');  
      cy.get('.ag-body-viewport').scrollTo('top');
      cy.wait(3000);
      cy.log('Validating the first F1 bit from top of the table');
      // cy.get('[row-index="1"] > [aria-colindex="4"]').should('have.text','•');
      cy.get('[row-index="1"] > [aria-colindex="4"]').invoke('text').then((text) => {
        cy.log('The transmitting bit of F1 is ', text)
        if (text == '•') {
          expect(text).to.have.string('•');
        } else if (text == '1') {
          expect(text).to.have.string('1');
        } else {
          cy.log('Value is neither 0 or 1. See the actual value -->', text);
          expect(true).to.be.false;
        }
      });
      cy.log('Validating the first G1 bit from top of the table');
      cy.get('[row-index="1"] > [aria-colindex="23"]').invoke('text').then((text) => {
        cy.log('The transmitting bit of G1 is ', text)
        if (text == '•') {
          expect(text).to.have.string('•');
        } else if (text == '1') {
          expect(text).to.have.string('1');
        } else {
          cy.log('Value is neither 0 or 1. See the actual value -->', text);
          expect(true).to.be.false;
        }
      });
  
    });


    it('Part1:5 Verify that the grid data is shown in descending time order when viewing records from top to bottom ', function () {
      cy.log('Verify that the grid data is shown in descending time order when viewing records from top to bottom');
      cy.log(': RTO-1433 steps 10,11,12')
      //Getting the time stamp from first row
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');      
      var time1;
      var time2;
      var time3;
      var date = new Date().toISOString().slice(0, 10);
      cy.get('[row-index="1"] > [aria-colindex="1"]').invoke('text').then((time) => {
        cy.log('The time at row 1 is  ', time);
        time1 = time;
  
        //Getting the time stamp from second row
        cy.get('[row-index="2"] > [aria-colindex="1"]').invoke('text').then((time) => {
          cy.log('The time at row 2 is ', time);
          time2 = time;

          //Getting the time stamp from third row
          cy.get('[row-index="3"] > [aria-colindex="1"]').invoke('text').then((time) => {
            cy.log('The time at row 3 is ', time);
            time3 = time;
    
            var date1 = new Date(date + "T" + time1 + "Z");
            var date2 = new Date(date + "T" + time2 + "Z");
            var date3 = new Date(date + "T" + time3 + "Z");
    
            cy.log("date1 is " + date1);
            cy.log("date2 is " + date2);
            cy.log("date3 is " + date3);
    
            //Convert to UNIX Time Stamp for accurate comparision
            var validation = (date1.getTime() > date2.getTime()) && (date2.getTime() > date3.getTime());
            if (validation) {
              expect(true).to.be.true;
            }
            else {
              cy.log("Issue with time sort in OMON");
              expect(true).to.be.false;
            }
          });
        });
      });
  
    });

    it('Part1:6 Verify default selected filter is Outstation ', function () {
      cy.log('Verify default selected filter is Outstation');
      common.closeNamedTabIfExists('OMON X04/003');
      omon.openOMON('X04/003');  
      cy.get('[aria-pressed="true"]').contains('X04/003').should('be.visible');
      common.closeTab();
    });

    
    it('Part1:7 Verify OMON Time display', function () {
      cy.log('Part1:7 Verify OMON Time display: RTO-1433 step 4');
      cy.log('Expected format = 08/09/2021, 20:14:22');
      common.closeNamedTabIfExists('OMON');
      cy.fixture('omon.json').then((equipment)=>{ 
        omon.openOMON(equipment.Junction2);  
        cy.get('[data-auto-id="server-time-header"]').invoke('text').then((text) => {          

          // get the current date and time
          var currentDate = new Date();

          // get the current date and time 5 seconds ago
          var dateMinus5secs = new Date();
          dateMinus5secs.setTime(dateMinus5secs.getTime() - 5000);

          var dateTimeString = currentDate.toLocaleDateString() + ', ' + currentDate.toTimeString();
          dateTimeString = dateTimeString.substring(0, 20); // cut string down to dd/mm/yyyy, hh:mm:ss

          var dateTimeStringMinus5 = dateMinus5secs.toLocaleDateString() + ', ' + dateMinus5secs.toTimeString();
          dateTimeStringMinus5 = dateTimeStringMinus5.substring(0, 20); // cut string down to dd/mm/yyyy, hh:mm:ss

          cy.log('Expected date & time = ' + dateTimeString);
          cy.log('Expected date & time minus 5 seconds = ' + dateTimeStringMinus5);
          cy.log('Displayed date and time = ' + text);

          // extract components from the date/time displayed 
          var day = parseInt(text.substring(0, 2),10);
          cy.log('Day = ' + day);
          
          var month = parseInt(text.substring(3, 5),10);
          cy.log('month = ' + month);
          
          var year = parseInt(text.substring(6, 10),10);
          cy.log('year = ' + year);
          
          var hour = parseInt(text.substring(12, 14),10);
          cy.log('hour = ' + hour);
          
          var minute = parseInt(text.substring(15, 17),10);
          cy.log('minute = ' + minute);
          
          var second = parseInt(text.substring(18, 20),10);
          cy.log('second = ' + second);

          // and create a Date object from it
          var displayedDate = new Date(year, month-1, day, hour, minute, second, 0);
          var displayedDateTimeString = displayedDate.toLocaleDateString() + ', ' + displayedDate.toTimeString();
          cy.log('calculated D & T string = ' + displayedDateTimeString);
          
          // check the displayed date is within 5 seconds of the current date and time
          var result = (displayedDate.getTime() <= currentDate.getTime()) && (displayedDate.getTime() >= dateMinus5secs.getTime());

          cy.log('result of date and time check = ' + result);
          cy.expect(result).to.eql(true); 
        });
        common.closeTab();
      });
    });*/

  });


