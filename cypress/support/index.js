// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
//require('cypress-xpath');

/*
import './commands';
import addContext from 'mochawesome/addContext';
// Alternatively you can use CommonJS syntax:
// require('./commands')
console.log('Setting up the cookies');
Cypress.Cookies.defaults({
  preserve: [
    'KEYCLOAK_IDENTITY',
    'KEYCLOAK_IDENTITY_LEGACY',
    'KEYCLOAK_LOCALE',
    'KEYCLOAK_REMEMBER_ME',
    'KEYCLOAK_SESSION',
    'KEYCLOAK_SESSION_LEGACY',
    'XSRF-TOKEN',
    'id',
    'AUTH_SESSION_ID',
    'AUTH_SESSION_ID_LEGACY',
    'AWSELBAuthSessionCookie-0',
    'AWSELBAuthSessionCookie-1',
    'RTOCookie-0',
    'RTOCookie-1',
  ],
});

Cypress.on('test:after:run', (test, runnable) => {
  if (test.state === 'failed') {
    let item = runnable;
    const nameParts = [runnable.title];

    // Iterate through all parents and grab the titles
    while (item.parent) {
      nameParts.unshift(item.parent.title);
      item = item.parent;
    }

    const fullTestName = nameParts.filter(Boolean).join(' -- '); // this is how cypress joins the test title fragments

    const imageUrl = `screenshots/${Cypress.spec.name}/${fullTestName} (failed).png`;

    addContext({ test }, imageUrl);
  }
});*/
