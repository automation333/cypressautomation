class common{

    closeTab(){
        cy.get('[data-auto-id="tab-2"] > [data-auto-id="close-tab-button"]').click();
    }


    closeNamedTabIfExists(tabName){
        /// <summary> 
        /// Closes a named tab, if it exists 
        /// </summary>
        let selectorString = '[title^="' + tabName + '"] > [data-auto-id="close-tab-button"]';

        cy.get("body").then($body => {
            if ($body.find(selectorString).length > 0) {   
                cy.get(selectorString).click();
            }
        });                      
    }

    openNamedTabIfExists(tabName){
        /// <summary> 
        /// Closes a named tab, if it exists 
        /// </summary>
        let selectorString = '#top-tabs > .its-tabs > .its-tabs__tab-group > [title^="' + tabName + '"]';

        cy.get("body").then($body => {
            if ($body.find(selectorString).length > 0) {   
                cy.get(selectorString).click();
            }
        });                      
    }

    closeTab2(){
        cy.get('[data-auto-id="tab-3"] > [data-auto-id="close-tab-button"]').click();
    }

    clickFirstTab(){
        cy.get('#top-tabs > .its-tabs > .its-tabs__tab-group > [data-auto-id="tab-2"]').click();
    }

    clickSecondTab(){
        cy.get('#top-tabs > .its-tabs > .its-tabs__tab-group > [data-auto-id="tab-3"]').click();
    }

    runCommand(command){
        cy.get("#cmd-entry-input").type(command +" {enter}");
    }
}

const commonPo = new common();
export default commonPo;