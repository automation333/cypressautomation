class equipmentSummary {

    clickEquipmentSummary() {
        cy.get('#equipmentsummary').click();
    }

    searchForEquipment(scn) {
        cy.get('#mat-input-1').clear().type(scn);
    }

    clickGo() {
        cy.get('[data-auto-id="equipment-search-control-go-button"]').click();
    }

    getequipmentList() {
        return cy.get('[data-auto-id="associated-scn-link"]').each(($item, index) => {
            cy.log('Scns associated to OTU are ',  $item.text());
        }).then(($items) => {
            return Cypress.$.makeArray($items).map(el => el.innerText);
        });
    }


    getSingleAssociatedEquipment() {
        return cy.get('[data-auto-id="associated-scn-link"]').first().invoke('text');
    }

}

const equipmentSummaryPo = new equipmentSummary();
export default equipmentSummaryPo;