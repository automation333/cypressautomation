class oovr{

clickNoReplyDropdown(){
    cy.get(".its-flyout__item").contains(' R: No reply ').click();
}

clickOverrideButton(){
    cy.get('[title="Override (F1)"]').click();
}

clickTransmissionErrorDropdown(){
    cy.get(".its-dropdown").click();
}

clickcancelOverrideButton(){
    cy.get('[data-auto-id="outstation-override-cancel-button"]').click();
}

}


const oovrPo = new oovr();
export default oovrPo;