class utcLogin{

    fillUsername(userName){
        cy.get('#username').type(userName);
    }

    fillPassword(password){
        cy.get('#password').type(password);
    }

    submit(){
        cy.get('.its-button').click();
    }


    login(){
        cy.log('Logging into the UTC system');
        //cy.visit(Cypress.env('login'));
        cy.visit('https://localhost/utc');
        cy.fixture('login.json').then((user)=>{
          this.fillUsername(user.username);
          this.fillPassword(user.password);
          this.submit();
        });
        cy.get('.its-application__header-title').should('be.visible');
        cy.log("User logged in succesfully");
    }



}

const loginPO= new utcLogin();


export default loginPO;